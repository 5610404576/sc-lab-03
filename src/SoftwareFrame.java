import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class SoftwareFrame extends JFrame {
	JFrame frame;
	JLabel label1,label2;
	JPanel panel1,panel2,panel3;
	JTextArea input1,result;
	JTextField input2;
	JButton enterbut,endbut ;
	JRadioButton wordsub,ngram;
	String str;
	
	public SoftwareFrame(){
		createFrame();
	}
	public void createFrame(){
		panel1 = new JPanel();
		panel2 = new JPanel();
		panel3 = new JPanel();
		label1 = new JLabel("Enter the words");
		label2 = new JLabel("Enter n-gram");
		input1 = new JTextArea(5,30);
		input2 = new JTextField(12);
		result = new JTextArea(5,20);
		enterbut = new JButton("Enter");
		endbut = new JButton("End");
		wordsub = new JRadioButton("Split");
		ngram = new JRadioButton("N-Gram");
		
		ButtonGroup group = new ButtonGroup();
		group.add(wordsub);
	    group.add(ngram);

	    panel1.setLayout(new GridLayout(4,1));
		panel1.add(label1);
		panel1.add(input1);
		panel1.add(label2);
		panel1.add(input2);
		
		panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
		panel2.add(wordsub);
		panel2.add(ngram);
		panel2.add(result);
		
		panel3.add(enterbut);
		panel3.add(endbut);
		
		setLayout(new BorderLayout());
		add(panel1,BorderLayout.WEST);
		add(panel2,BorderLayout.EAST);
		add(panel3,BorderLayout.SOUTH);
		
		
		result.setEditable(false);
		input2.disable();
	}
	
	public void setResult(String str){
		this.str = str;
		result.setText(str);
	}

	  public String getInput1(){
		  return input1.getText();
	   }
	  public String getInput2(){
		  return input2.getText();
	   }
	  public void showNgram(){
		  label2.enable();
		  input2.enable();
	  }
	  public void notshowNgram(){
		  input2.disable();
	  }
	 public void setListener(ActionListener list) {
		 endbut.addActionListener(list);
	   }
	 public void setListener2(ActionListener list) {
		 enterbut.addActionListener(list);
	   }
	 public void setListener3(ActionListener list) {
		 ngram.addActionListener(list);
	   }
	 public void setListener4(ActionListener list) {
		 wordsub.addActionListener(list);
	   }
}
