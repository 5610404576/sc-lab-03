import java.util.ArrayList;
import java.util.StringTokenizer;


public class StringNgram {
	
	public String WordSub(String str){
		ArrayList<String> list = new ArrayList<String>();
		StringTokenizer token = new StringTokenizer(str);
		String word="";
		String n="";
		while(token.hasMoreElements()){
			list.add(token.nextToken());
		}
		for (int i = 0; i < list.size(); i++) {
			n = "size:"+list.size()+"\n";
			word = word + (i + 1) + ". " + list.get(i) + "\n";
		}
		return n+word;		
	}
	
	public String generateNgram(String str,String count){
		int n = Integer.parseInt(count);
		String str2 = "";
		String result = "";
		StringTokenizer token = new StringTokenizer(str);
		while (token.hasMoreElements()){
			str2 += token.nextToken();
		}
		for (int i=0;i<=str2.length()-n;i++){
			result += str2.substring(i,i+n);
			result += " ";
		}
		return result;
	}
}
