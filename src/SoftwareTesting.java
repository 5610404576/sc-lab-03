import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;


public class SoftwareTesting {
	
	 public static void main(String[] args) {
	         new SoftwareTesting();
		}
	 
	 public SoftwareTesting() {
			frame = new SoftwareFrame();
			model = new StringNgram();
	        frame.pack();
	        frame.setVisible(true);
	        frame.setSize(600,400);
	        	    
	        frame.setListener(new ActionListener() {
				   
		        public void actionPerformed(ActionEvent event){
		        	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					System.exit(0);		   	               	
		         }            
			});
	        
	        frame.setListener2(new ActionListener() {
				   
		        public void actionPerformed(ActionEvent event){
		        	if(frame.ngram.isSelected()){
		        		frame.setResult(model.generateNgram(frame.getInput1(), frame.getInput2()));
		        	}
		        	else{
		        		frame.setResult(model.WordSub(frame.getInput1()));
		        	}
		        			   	             
		         }            
			});
	        
	        frame.setListener3(new ActionListener() {
				   
		        public void actionPerformed(ActionEvent event){
		        		frame.showNgram();
		         }     

			});
	        
	        frame.setListener4(new ActionListener() {
				   
		        public void actionPerformed(ActionEvent event){
				        frame.notshowNgram();
		         }     

			});

		}
	 
	 SoftwareFrame frame;
	 ActionListener list;
	 StringNgram model;
}
